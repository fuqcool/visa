const https = require('https')
const fs = require('fs')
const url = require('url')
const accountSid = process.env.ACCOUNT_SID
const authToken = process.env.AUTH_TOKEN
const client = require('twilio')(accountSid, authToken)

const port = 8080

var options = {
    key: fs.readFileSync( './server.key' ),
    cert: fs.readFileSync( './server.cert' ),
    requestCert: false,
    rejectUnauthorized: false
}

const server = https.createServer(options, (req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.writeHead(200)
    res.end()

    const q = url.parse(req.url, true).query

    if (!q.message) {
        return
    }

    if (q.log) {
        console.log(`${new Date().toLocaleString()} ${q.message}`)
    } else {
        sendMessage(q.message)
    }
})

server.listen(port, () => {
    console.log(`Server running at https://localhost:${port}/`)
})

function sendMessage(body) {
    client.messages
          .create({
              body: body,
              from: '+18575765510',
              to: '+17815026132'
          })
          .then(message => console.log(`${new Date().toLocaleString()} [Message] ${body}`))
          .catch(err => console.error(err.message))
}
