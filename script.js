(function () {
    let lastDates = 'initialvalue';

    const intervalId = setInterval(() => {
        fetch('https://ais.usvisa-info.com/en-ca/niv/schedule/26972520/appointment/days/95.json?appointments[expedite]=false')
            .then(res => {
                if (res.status == 401) {
                    fetch('https://localhost:3008/?message=Your+session+has+expired');
                    clearInterval(intervalId);
                    return;
                }

                res.json().then(data => {
                    if (data.length) {
                        const dates = data.slice(0, 10).map(d => d.date).join(', ');
                        const msg = encodeURIComponent(`Found appointment slots on ${dates}`);

                        if (dates === lastDates) {
                            fetch(`https://localhost:3008/?message=${msg}&log=1`);
                            return;
                        }
                        lastDates = dates;
                        fetch(`https://localhost:3008/?message=${msg}`);
                    } else {
                        if (lastDates) {
                            fetch('https://localhost:3008/?message=No+appointments+available');
                            lastDates = '';
                        } else {
                            fetch('https://localhost:3008/?log=1&message=No+appointments+available');
                        }
                    }
                });
            });
    }, 60000);
}());
